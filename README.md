# cs-archival
##  Flask-based Google Cloud Function for BigQuery and GCP bucket
This Flask-based Google Cloud Function listens for a new csv file uploaded to a Google bucket and performs the following actions:

Creates a temporary table in BigQuery for the file data using a custom schema.
Inserts the data into an archive table using a custom insert query.
Drops the temporary table.
This code includes error handling and GCP logging capabilities, and assumes that the archive table already exists in BigQuery.

##  Prerequisites
A Google Cloud Platform (GCP) account with the necessary permissions to create a Google Cloud Function and access a BigQuery dataset.
A Google Cloud Storage (GCS) bucket to store the csv file.
A BigQuery dataset with an existing archive table.
## Configuration
Clone this repository to your local machine.
Open the main.py file and specify the following variables:
BUCKET_NAME: the name of the GCS bucket where the csv file will be uploaded.
ARCHIVE_TABLE: the name of the existing archive table in BigQuery where the data will be inserted.
dataset_id: the name of the BigQuery dataset that contains the archive table.
Modify the custom insert query as needed to match the schema of the archive table.
Deploy the Google Cloud Function using the GCP Console or the Google Cloud SDK.
## Usage
Upload a csv file to the GCS bucket.
The Google Cloud Function will trigger automatically, process the csv file, insert the data into the archive table, and drop the temporary table.
Monitor the logs in the GCP Console to ensure that the function is running successfully.
## Error Handling
In the event of an error, the function will log an error message and terminate. Check the logs in the GCP Console for information on the error.



## Getting Started
1. Clone this repository to your local machine
2. Create a virtual environment and activate it
```
python3 -m venv env
source env/bin/activate

```
3. Install the required packages
```
pip install -r requirements.txt

```
4. Create a Google Cloud Project and enable the BigQuery and Cloud Functions APIs
5. Set up the Google Cloud SDK and authenticate using your Google account
```
gcloud auth login


```
6. Set the default project to your Google Cloud Project

```
gcloud config set project [PROJECT_ID]

```
7. Create the Cloud Function and deploy the code to it
```
gcloud functions deploy [FUNCTION_NAME] --runtime python310 --trigger-bucket [BUCKET_NAME] --entry-point process_csv


```
