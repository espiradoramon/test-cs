FROM google/cloud-sdk:latest

ADD . / app/

RUN chmod +x app/deploy.sh

CMD ["/bin/sh", "app/deploy.sh"]
