import os
import logging
import io
import sys


from flask import Flask
from google.cloud import bigquery
from google.cloud import storage
from google.cloud.bigquery import SchemaField
import tempfile





log = logging.getLogger(__name__)
def init_log(log):
    logFormatter = logging.Formatter(
        '[%(asctime)s] [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)2s] %(message)s')

    stdLogger = logging.StreamHandler(sys.stdout)
    stdLogger.setFormatter(logFormatter)
    log.addHandler(stdLogger)

    log.setLevel(logging.DEBUG)

    return log

log = init_log(log)







app = Flask(__name__)

CUSTOM_SCHEMA = [
              bigquery.SchemaField('employee_id', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.SchemaField('employee_name', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('employee_site', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('employee_team', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('employee_tier', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('verint_organization', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('designation', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('start_date', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('tool', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('tool_id', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('contact_type', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('queue_language', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('queue_skill', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('segments', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('queue', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('year', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('month', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.  SchemaField('week', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('day', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('date', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('hourly_interval', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.SchemaField('handled', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('abandoned', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('outbound', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('accept_transferred_out', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('handle_time', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('wait_time', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('abandon_time', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('outbound_time', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('time', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('contact_id', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('originator', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('next_address', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('activity_code', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.SchemaField('cfr_username', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.SchemaField('cfr_customer_verified', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('cfr_product', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('cfr_inquiry', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('cfr_sub_inquiry', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.SchemaField('cfr_ip', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('cfr_country', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('cfr_source', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('cfr_referrer', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('cfr_restart', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('cfr_bet_slip_callback_id', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('cfr_custom_data', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.  SchemaField('brand', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('product', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.  SchemaField('query_type', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.SchemaField('handled_excl_email', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('interval_thirty', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('topic_name', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery.SchemaField('key_word1', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('key_word2', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('key_word3', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('sata_usage', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('daily_staffed_time', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('hourly_staffed_time', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('sla_ans', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('sla_abn', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('en_row_filter', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('blank1', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('social_app', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('queue_tier', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery. SchemaField('interval_five', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('country_look_up', 'STRING', 'NULLABLE', None, None, (), None), 
              bigquery.SchemaField('route_point', 'STRING', 'NULLABLE', None, None, (), None),
              bigquery. SchemaField('application_name', 'STRING', 'NULLABLE', None, None, (), None),
]

ARCHIVE_TABLE = "ornate-producer-230307.upload.test"

@app.route('/process_csv', methods=['POST'])
def process_csv(data,context):
    try:
        # Create BigQuery client
        bigquery_client = bigquery.Client()
        storage_client = storage.Client()

        # Get the latest CSV file from the Google bucket
        bucket = storage_client.get_bucket("test-cs-up")
        blobs = bucket.list_blobs()
        latest_blob = max(blobs, key=lambda x: x.updated)

# Download the latest CSV file to a local file
        with tempfile.NamedTemporaryFile(mode="wb", delete=False) as temp:
             latest_blob.download_to_file(temp)
             temp.flush()

# Create a table name using the latest CSV file name
        table_id = latest_blob.name.replace(".csv", "")

# Create a temporary table in BigQuery using the table name and custom schema
        dataset_id = "upload"
        table_ref = bigquery_client.dataset(dataset_id).table(table_id)
        table = bigquery.Table(table_ref)
        table.schema = CUSTOM_SCHEMA
        bigquery_client.create_table(table)

# Load the data from the latest CSV file into the temporary table
        csv_file = io.StringIO(latest_blob.download_as_string().decode("utf-8"))
        job_config = bigquery.LoadJobConfig(source_format="CSV")
        job = bigquery_client.load_table_from_file(
            csv_file, table_ref, job_config=job_config
               )
        job.result()


        # Insert the data from the temporary table into the archive table
        query = f"INSERT INTO `{ARCHIVE_TABLE}`  SELECT employee_id, employee_name,employee_site,employee_team,employee_tier,verint_organization, designation,start_date,tool,tool_id,contact_type,queue_language,queue_skill,segments,queue,year,month,week,day,CAST(date AS STRING) AS date,hourly_interval,abandoned,outbound, handled,accept_transferred_out,handle_time,wait_time, abandon_time,outbound_time,time,contact_id,originator,next_address,activity_code,cfr_username, cfr_customer_verified,cfr_product,cfr_inquiry,cfr_sub_inquiry,cfr_ip, cfr_country,cfr_source,cfr_referrer,cfr_restart,cfr_bet_slip_callback_id,cfr_custom_data, brand,product,query_type,handled_excl_email,interval_thirty,topic_name, key_word1,key_word2,key_word3,sata_usage,daily_staffed_time,hourly_staffed_time,sla_ans,sla_abn,en_row_filter,blank1,social_app,queue_tier,interval_five,country_look_up,route_point,application_name FROM `{table_ref}`"
        query_job = bigquery_client.query(query)
        query_job.result()

        # Drop the temporary table
        bigquery_client.delete_table(table)

        # Log success message
        logging.info(f"Successfully processed CSV file: {latest_blob.name}")
        return "Success", 200
    except Exception as e:
        # Log error message
        logging.error(f"Error processing CSV file: {str(e)}")
        return "Error", 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
