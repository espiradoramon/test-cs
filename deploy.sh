#!/bin/bash
set +x
set -e

echo "Executing parse yaml script.."
. app/parse_yaml.sh
eval $(parse_yaml app/.ci/pipelines/$1 "CONF_")

echo "Activing service account.."
gcloud auth activate-service-account --key-file app/ornate-producer-230307-f99d3fcaf1cd.json

cd app/
echo "Deploying the service.."
gcloud --quiet --project $CONF_ENV_PROJECT_ID functions deploy $CONF_ENV_FUNC_SERIES --max-instances $CONF_ENV_MAX_INSTANCES --runtime $CONF_ENV_RUNTIME --trigger-bucket=test-cs-up --entry-point $CONF_ENV_ENTRY_POINT --set-env-vars PROJECT_ID=$CONF_ENV_PROJECT_ID --memory=$CONF_ENV_MEMORY --region=$CONF_ENV_REGION --verbosity=info

echo "Script has successfully finished!"
